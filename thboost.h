/* threadboost.h
 *
 * Copyright (C) 2019-2020 Aaron Erhardt
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __THBOOST
#define __THBOOST

#include <pthread.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* ==============================  ABOUT THREADBOOST  ============================
 *
 * Threadboost is a highly optimized thread pool that aims to run as fast as possible.
 * In certain situations in can run over 200 times faster than regular thread pools
 * yet its design might have some drawbacks for certain applications.
 */


/* ================================ STRUCTURES =================================== */

/* >>> worker_job_t
 *
 * Stores two pointers to the function and the argument a worker thread needs
 * to run its assgigned job.
 */

typedef struct worker_job_t {
  void (*volatile func)(void *); /* Function a worker thread will execute */
  void volatile *args;           /* Arguments used for the function */
} worker_job_t;


/* >>> worker_args_t
 *
 * Stores the pointers needed to initialize the worker threads,
 * used in the function worker_loop.
 */

typedef struct worker_args_t {
  worker_job_t *job;

  uint_fast8_t volatile *start_condition;
  uint_fast8_t volatile *stop_condition;
  uint_fast8_t volatile *exit_condition;
} worker_args_t;


/* >>> thboost_t
 *
 * Stores all information related to the threadpool and required by the API.
 */

typedef struct thboost_t {
  pthread_t *threads;
  pthread_attr_t th_attr;
  unsigned int num_of_threads;

  uint_fast8_t volatile start_condition;
  uint_fast8_t volatile exit_condition;
  uint_fast8_t volatile *stop_conditions;

  uint_fast8_t volatile *last_stop_condition;

  worker_job_t *main_job;

  worker_job_t *jobs;

  worker_args_t *worker_args;
} thboost_t;


/* ==================================  THE API  ================================== */


/* >>> thboost_t* thboost_init(const unsigned int num_of_threads)
 *
 * DESCRIPTION: returns a new thread pool with the specified number of worker threads.
 *
 * RETURN:      a new threadpool.
 *
 * ARGUMENT:    the number of worker threads,
 *              recommended maximum is one less than the number of CPU cores.
 */

thboost_t* thboost_init(const unsigned int num_of_threads);


/* >>> void thboost_destroy(thboost_t *const th_pool);
 *
 * DESCRIPTION: destroys a given thread pool and frees all resources.
 *
 * ARGUMENT:    pointer to the thread pool that should be destroyed.
 */

void thboost_destroy(thboost_t* const th_pool);


/* >>> void thboost_work(thboost_t *const th_pool);
 *
 * DESCRIPTION: lets all worker threads do their assigned work,
 *              returns when all worker threads are finished.
 *
 * ARGUMENT:    pointer to the thread pool that should do the work.
 */

void thboost_work(thboost_t *const th_pool);


/* >>> void thboost_add_work(thboost_t *const th_pool, const unsigned int thread_num, void *const func, void *const args);
 *
 * DESCRIPTION: Assigns work (a function + argument) to a specific worker thread.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker thread
 *            - thread_num: number of the worker thread (0 is the first, 1 the second etc.)
 *            - func: function pointer assigned to the worker thread
 *            - args: argument, the assigned function should take (pass multiple arguments as a struct)
 */

void thboost_add_work(thboost_t *const th_pool, const unsigned int thread_num, void *const func, void *const args);


/* >>> void thboost_add_work_all(thboost_t *const th_pool, void *const func, void *const args);
 *
 * DESCRIPTION: Assigns the same work (a function + argument) to all worker threads.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker threads
 *            - func: function pointer assigned to all worker threads
 *            - args: argument, the assigned function should take (pass multiple arguments as a struct)
 */

void thboost_add_work_all(thboost_t *const th_pool, void *const func, void *const args);


/* >>> void thboost_add_func(thboost_t *const th_pool, const unsigned int thread_num, void *const func);
 *
 * DESCRIPTION: Assigns a function to a specific worker thread.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker thread
 *            - thread_num: number of the worker thread (0 is the first, 1 the second etc.)
 *            - func: function pointer assigned to the worker thread
 */

void thboost_add_func(thboost_t *const th_pool, const unsigned int thread_num, void *const func);


/* >>> void thboost_add_func_all(thboost_t *const th_pool, void *const func);
 *
 * DESCRIPTION: Assigns the same function to all worker threads.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker threads
 *            - func: function pointer assigned to all worker threads
 */

void thboost_add_func_all(thboost_t *const th_pool, void *const func);


/* >>> void thboost_add_args(thboost_t *const th_pool, const unsigned int thread_num, void *const args);
 *
 * DESCRIPTION: Assigns a argument to a specific worker thread.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker thread
 *            - thread_num: number of the worker thread (0 is the first, 1 the second etc.)
 *            - args: argument, the assigned function should take (pass multiple arguments as a struct)
 */

void thboost_add_args(thboost_t *const th_pool, const unsigned int thread_num, void *const args);


/* >>> void thboost_add_args_all(thboost_t *const th_pool, void *const args);
 *
 * DESCRIPTION: Assigns the same argument to all worker threads.
 *
 * ARGUMENTS: - th_pool: pointer to the thread pool of the targeted worker threads
 *            - args: argument, the assigned function should take (pass multiple arguments as a struct)
 */

void thboost_add_args_all(thboost_t *const th_pool, void *const args);



/* ==============================  THE INTERNAL API  =============================
 *
 * The internal API provides the same functions as the default API (see above) except that the
 * functions have names that start with "thboost_internal..." instead of "thboost..."
 * and always work with the internal thread pool. Therefore you also never need to pass
 * a pointer to a thread pool to functions of the internal API.
 * Since the internal API behaves similar to the default API only a few points documented
 * in this section. For reference have look at the functions of the default API.
 */


/* >>> void thboost_internal_init(const unsigned int num_of_threads);
 *
 * NOTE: creates the internal thread pool similar to thboost_init.
 *       This function can be called multiple times but will create the internal
 *       thread pool only if it doesn't exist already.
 *       There can only be ONE INTERNAL THREAD POOL.
 */

void thboost_internal_init(const unsigned int num_of_threads);

void thboost_internal_work();

void thboost_internal_add_work(const unsigned int thread_num, void *const func, void *const args);

void thboost_internal_add_work_all(void *const func, void *const args);

void thboost_internal_add_func(const unsigned int thread_num, void *const func);

void thboost_internal_add_func_all(void *const func);

void thboost_internal_add_args(const unsigned int thread_num, void *const args);

void thboost_internal_add_args_all(void *const args);

void thboost_internal_destroy();

#ifdef __cplusplus
}
#endif

#endif

