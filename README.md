# THREADBOOST

## ABOUT
Threadboost is a highly optimized thread pool that aims to run as fast as possible.
In certain situations in can run over 200 times faster than regular thread pools yet its design might have some drawbacks for certain applications.

## DESIGN
In order to be as fast as possible Threadboost was designed to work unlike most other thread pools.
This means however that Threadboost - despite its high performance - is not the best choice for every project.

## PROBLEMS OF REGULAR THREAD POOLS

Calculating tasks in parallel is often faster than using a single thread.
The disadvantage of parallel calculations is however that you need to synchronize between the main and the worker threads.
If a task is small enough it is faster to run it on a single thread because it would take too long to communicate with all the worker threads to get them all running.
Threadboost tries to assign and do work as fast as possible in order to lose fewer time while communicating between threads.
Therefore even small tasks can be efficiently computed in parallel.

### ASSIGNING WORK
In Threadboost assigning work to the worker threads takes two steps:

1. Assigning a function and its argument to a worker thread.

2. Telling all worker threads to execute their assigned functions.

This means you can assign functions and arguments individually to specific worker threads but you can't run a worker thread individually.
Either you send all worker threads to work or you don't let them work at all.

Also Threadboost has no job queue.

### THREAD SYNCHRONIZATION
In order to synchronize between threads Threadboost uses simple loops to check for state changes.
This "spinning" as it is usually referred to is quite a bit faster than regular mutexes but comes with a downside:
while spinning all worker threads use as much CPU time as they can get and the scheduler usually doesn't know that they are just spinning and waiting for other threads to do something.
Therefore it might happen that the operating system schedules spinning threads before running still working threads. This can reduce the efficiency and speed of the thread pool drastically.
If you know however that you have some CPU cores dedicated to the thread pool that's usually no problem.

### IDEAL USE CASE
Summarizing all the points from above Threadboost works ideally for tasks that 
+ can be equally split across a fixed amount of threads.
+ can be run on multiple threads simultaneously.

## EXAMPLE
```c
#include <stdio.h>
#include "thboost.h"

#define NUM_OF_THREADS 5

void print_num(void * args)
{
  int* number = (int*)args;

  printf("NUMBER: %i\n", *number);
}

int main(int argc, char *argv[])
{
  /* creating new thread pool */
  thboost_t* thpool = thboost_init(NUM_OF_THREADS);

  int test[] = { 1, 2, 3, 4, 5 };

  /* assigning the function print_num and a number to each worker thread */
  for (long t = 0; t < NUM_OF_THREADS; ++t) {
    thboost_add_work(thpool, t, print_num, (void*)(&test[t]));
  }

  /* running all worker threads at once */
  thboost_work(thpool);

  /* destroying the thread pool */
  thboost_destroy(thpool);

  return 0;
}
```

## THE CORE API
```c
struct thboost_t
```
This struct stores all relevant information about the thread pool. It is not intended to be accessed manually.

----

```c
thboost_t* thboost_init(unsigned int num_of_threads);
```

+ DESCRIPTION: returns a new thread pool with the specified number of worker threads.
+ RETURN: a new thread pool.
+ ARGUMENT: the number of worker threads, recommended maximum is one less than the number of CPU cores.

----

```c
void thboost_destroy(thboost_t* th_pool);
```
* DESCRIPTION: destroys a given thread pool and frees all resources.
* ARGUMENT: pointer to the thread pool that should be destroyed.

----

```c
void thboost_work(thboost_t* th_pool);
```
* DESCRIPTION: lets all worker threads do their assigned work, returns when all worker threads are finished.
* ARGUMENT: pointer to the thread pool that should do the work

----

```c
void thboost_add_work(thboost_t* th_pool, unsigned int thread_num, void* func, void* args);
```
* DESCRIPTION: Assigns work (a function + argument) to a specific worker thread.
* ARGUMENTS: 

  - th_pool: pointer to the thread pool of the targeted worker thread. 
  - thread_num: number of the worker thread, 0 is the first, 1 the second etc.
  - func: function pointer assigned to the worker thread.
  - args: argument, the assigned function should take, pass multiple arguments as a struct
  
----

__Further API functions are described inside `thboost.h`.__

## THE INTERNAL API
To make it easier to access a global thread pool across multiple files Threadboost also offers an internal API.
All regular functions have a representation that starts with "thboost_internal" instead of "thboost" and automatically accesses the internal thread pool.
More information can be found inside `thboost.h`.
