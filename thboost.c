/* threadboost.c
 *
 * Copyright (c) 2019-2020 Aaron Erhardt
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#include "thboost.h"


/* =============================  DEBUG FLAGS  ============================ */

/*
 * THBOOST_DEBUG:
 *   Enables printing of error messages (to stderr).
 *
 * THBOOST_INFO_MAIN
 *   Enables printing of useful information about the status of the main thread.
 *
 * THBOOST_INFO_THREAD
 *   Enables printing of useful information about the status of the worker threads.
 *
 * THBOOST_INFO_ALL
 *   Enables both THBOOST_INFO_MAIN and THBOOST_INFO_THREAD.
 *
 * THBOOST_TIME_INFO
 *   Enables printing of time information of the worker threads.
 *   This includes the times spent for executing the assigned worker function and
 *   a whole worker loop.
 *
 * THBOOST_CPU_STATS
 *   Enables collecting and printing of information about the CPU time and the real
 *   time a worker thread uses. If the CPU time is significantly lower than the real
 *   time you should probably reduce the amount of worker threads and background
 *   processes on your system.
 * */

/* UNCOMMNENT THE FLAGS YOU NEED HERE */
//#define THBOOST_DEBUG
//#define THBOOST_INFO_MAIN
//#define THBOOST_INFO_THREAD
//#define THBOOST_INFO_ALL
//#define THBOOST_TIME_INFO
//#define THBOOST_CPU_STATS


/* ===========================  OPTIONAL DELAY  =========================== */

/* Uncomment (and edit) the following section to add a delay to the spinning loops
 * of the thread pool in order to avoid high CPU usage caused by the spinning of the threads.
 * But keep in mind that this will HARM THE PERFORMANCE.
*/
/*
 #include <time.h>
   static struct timespec THBOOST_DELAY = {.tv_sec = 0, .tv_nsec = 1};
 #define THBOOST_EVENT_DELAY() nanosleep(&THBOOST_DELAY, NULL)
 */

/* ============================  PREPROCESSOR  ============================ */


#if defined(THBOOST_DEBUG) || defined(THBOOST_INFO) ||                         \
    defined(THBOOST_TIME_INFO) || defined(THBOOST_CPU_STATS)
#include <stdio.h>
#endif

#if defined(THBOOST_DEBUG)
#define err(str)                                                               \
  fprintf(stderr, "line %d: %s() -> %s\n", __LINE__, __func__, str)
#else
#define err(str)
#endif

#if defined(THBOOST_INFO_MAIN) || defined(THBOOST_INFO_ALL)
#define INFO_MAIN(str) printf("MAIN: " str "\n")
#else
#define INFO_MAIN(str)
#endif

#if defined(THBOOST_INFO_THREAD) || defined(THBOOST_INFO_ALL)
#define INFO_THREAD(str) printf("THREAD: " str "\n")
#else
#define INFO_THREAD(str)
#endif

#if defined(THBOOST_TIME_INFO)
#define INFO_TIME(str, time) printf("TIME: " str " %ld\n", time)
#else
#define INFO_TIME(str)
#endif

/* ========================== MACROS ============================ */

/* easily allocate memory and avoid errors with automatic type casting */
#define easy_malloc(data_type, num_of_elems) \
        (data_type *) malloc(sizeof(data_type) * num_of_elems)

/* ========================== STATIC FUNCTIONS ============================ */

static void worker_loop(void *worker_args);
static void empty_func(void *empty_args) {}

/* ========================== THREADBOOST ============================ */

thboost_t *thboost_init(const unsigned int num_of_threads) {

  /* "num_of_workers" is one less than the actual num_of_threads
   * because the main thread also acts like a worker thread.*/
  const unsigned int num_of_workers = num_of_threads - 1;


  INFO_MAIN("Allocating memory for the thboost struct");

  thboost_t *const th_pool = easy_malloc(thboost_t, 1);

  if (th_pool == NULL) {
    err("Could not allocate memory for thboost structure");
    return NULL;
  }

  th_pool->num_of_threads = num_of_threads;
  th_pool->exit_condition = 0;
  th_pool->start_condition = 0;

  /* Set the pthread attribute to make all new threads detached in order
   * to save up system resources */
  pthread_attr_init(&th_pool->th_attr);
  pthread_attr_setdetachstate(&th_pool->th_attr, PTHREAD_CREATE_DETACHED);


  INFO_MAIN("Allocating memory for the worker threads");

  th_pool->threads = easy_malloc(pthread_t, num_of_workers);
  if (th_pool->threads == NULL) {
    err("Could not allocate memory for the worker threads");
    free(th_pool);
    return NULL;
  }


  INFO_MAIN("Allocating memory for the stop conditions of the worker threads");

  th_pool->stop_conditions = easy_malloc(uint_fast8_t, num_of_workers);
  if (th_pool->stop_conditions == NULL) {
    err("Could not allocate memory for stop conditions of the worker threads");
    free(th_pool->threads);
    free(th_pool);
    return NULL;
  }


  INFO_MAIN("Allocating memory for the jobs of the worker threads");

  th_pool->jobs = easy_malloc(worker_job_t, num_of_threads);
  if (th_pool->jobs == NULL) {
    err("Could not allocate memory for the jobs of the worker threads");
    free(th_pool->threads);
    free((void *)th_pool->stop_conditions);
    free(th_pool);
    return NULL;
  }


  INFO_MAIN("Allocating memory for the worker arguments");

  th_pool->worker_args = easy_malloc(worker_args_t, num_of_workers);

  if (th_pool->worker_args == NULL) {
    err("Could not allocate memory for worker arguments");
    free(th_pool->threads);
    free((void *)th_pool->stop_conditions);
    free(th_pool->jobs);
    free(th_pool);
    return NULL;
  }

  // first thread is MAIN (thread No 0), so no worker thread needs to be spawned
  th_pool->jobs[0].args = NULL;
  th_pool->jobs[0].func = empty_func;

  th_pool->main_job = &th_pool->jobs[0];


  INFO_MAIN("Initializing worker threads");

  int rc;
  for (uint_fast8_t t = 0; t < num_of_workers; ++t) {

    /* Initialize worker threads to execute empty_func with argument "NULL" */
    th_pool->jobs[t + 1].args = NULL;
    th_pool->jobs[t + 1].func = empty_func;

    /* Initialize worker args with all info the worker thread needs */
    th_pool->worker_args[t].job = &th_pool->jobs[t + 1];
    th_pool->worker_args[t].stop_condition = &th_pool->stop_conditions[t];
    th_pool->worker_args[t].start_condition = &th_pool->start_condition;
    th_pool->worker_args[t].exit_condition = &th_pool->exit_condition;

    /* create pthreads */
    rc = pthread_create(&th_pool->threads[t], &th_pool->th_attr,
                        (void *)worker_loop, (void *)&th_pool->worker_args[t]);

    if (rc) {
      err("Couldn't create thread!");
      exit(-1);
    }
  }

  /* calculating the pointer of the last stop condition to optimize a loop in thboost_work */
  th_pool->last_stop_condition = th_pool->stop_conditions + num_of_workers;

  INFO_MAIN("thboost_init finished successfully");

  return th_pool;
}

void thboost_work(thboost_t *const th_pool) {

  #if defined(THBOOST_DEBUG)
    int count_waiting_iterations = 0;
  #endif

  INFO_MAIN("Waking up worker threads");
  th_pool->start_condition++;

  INFO_MAIN("Doing work");

  th_pool->main_job->func((void *)th_pool->main_job->args);

  INFO_MAIN("Finished work");

  register uint_fast8_t volatile *stop_condition = th_pool->stop_conditions;
  register uint_fast8_t volatile start_condition = th_pool->start_condition;

  while (stop_condition < th_pool->last_stop_condition) {

    INFO_MAIN("Waiting for worker threads to finish");

    while (*stop_condition != start_condition) {
      #if defined(THBOOST_DEBUG)
        count_waiting_iterations++;
      #endif

      #ifdef THBOOST_EVENT_DELAY
        THBOOST_EVENT_DELAY();
      #endif
    }
    stop_condition++;

  }


  #if defined(THBOOST_DEBUG)
    if (count_waiting_iterations > 10000) {
      err("WARNING: Slow worker threads: Maybe too many threads active!");
    }
  #endif

  INFO_MAIN("thboost_work finished successfully");
}

void thboost_destroy(thboost_t* const th_pool) {

  INFO_MAIN("Destroying threads");

  th_pool->exit_condition = 1;
  th_pool->start_condition++;

  for (uint_fast8_t volatile *stop_condition = th_pool->stop_conditions;
       stop_condition < th_pool->last_stop_condition;) {

    INFO_MAIN("Waiting for worker threads to exit");

    if (*stop_condition == th_pool->start_condition) {
      stop_condition++;

    } else {
      #ifdef THBOOST_EVENT_DELAY
        THBOOST_EVENT_DELAY();
      #endif
    }
  }

  INFO_MAIN("Freeing memory");

  free(th_pool->threads);

  free((void *)th_pool->stop_conditions);
  free(th_pool->jobs);
  free(th_pool->worker_args);

  pthread_attr_destroy(&th_pool->th_attr);

  free(th_pool);

  INFO_MAIN("thboost_destroy finished successfully");
}

void thboost_add_work(thboost_t *const th_pool, const unsigned int thread_num, void *const func,
                      void *const args) {
  th_pool->jobs[thread_num].func = func;
  th_pool->jobs[thread_num].args = args;

  INFO_MAIN("thboost_add_work finished successfully");
}

void thboost_add_work_all(thboost_t *const th_pool, void *const func, void *const args) {
  for (register uint_fast8_t t = 0; t < th_pool->num_of_threads; ++t) {
    th_pool->jobs[t].func = func;
    th_pool->jobs[t].args = args;
  }

  INFO_MAIN("thboost_add_work_all finished successfully");
}

void thboost_add_func(thboost_t *const th_pool, const unsigned int thread_num, void *const func) {
  th_pool->jobs[thread_num].func = func;

  INFO_MAIN("thboost_add_func finished successfully");
}

void thboost_add_func_all(thboost_t *const th_pool, void *const func) {
  for (register uint_fast8_t t = 0; t < th_pool->num_of_threads; ++t) {
    th_pool->jobs[t].func = func;
  }

  INFO_MAIN("thboost_add_func_all finished successfully");
}

void thboost_add_args(thboost_t *const th_pool, const unsigned int thread_num, void *const args) {
  th_pool->jobs[thread_num].args = args;

  INFO_MAIN("thboost_add_args finished successfully");
}

void thboost_add_args_all(thboost_t *const th_pool, void *const args) {
  for (register uint_fast8_t t = 0; t < th_pool->num_of_threads; ++t) {
    th_pool->jobs[t].args = args;
  }

  INFO_MAIN("thboost_add_args_all finished successfully");
}

/* ========================== HELPER FUNCTIONS ============================ */

static void worker_loop(void *worker_args) {

  #if defined(THBOOST_TIME_INFO)
    clock_t last_time = 0;
  #endif

  #if defined(THBOOST_CPU_STATS)
    struct timespec real_time;
    struct timespec cpu_time;

    clock_gettime(CLOCK_MONOTONIC, &real_time);
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &cpu_time);
  #endif

  INFO_THREAD("Worker loop initiated!");

  /* Type casting the data type of the argument */
  worker_args_t *const args = (worker_args_t *)worker_args;

  /* Storing important pointers in registers to speed up execution */
  register void (*volatile * func_ptr)(void *) = &args->job->func;
  register void *volatile *args_ptr = (void **)&args->job->args;

  register uint_fast8_t volatile *const start_condition_ptr = args->start_condition;
  register uint_fast8_t volatile *const stop_condition_ptr = args->stop_condition;
  register uint_fast8_t volatile *const exit_condition_ptr = args->exit_condition;

  *stop_condition_ptr = 0;

  for (;;) {
    if (*stop_condition_ptr != *start_condition_ptr) {
      if (*exit_condition_ptr) {
        INFO_THREAD("EXIT!");

        *stop_condition_ptr = *start_condition_ptr;

        #if defined(THBOOST_CPU_STATS)
          struct timespec real_time_now;
          struct timespec cpu_time_now;

          clock_gettime(CLOCK_MONOTONIC, &real_time_now);
          clock_gettime(CLOCK_THREAD_CPUTIME_ID, &cpu_time_now);

          real_time_now.tv_sec -= real_time.tv_sec;
          real_time_now.tv_nsec -= real_time.tv_nsec;

          if (real_time_now.tv_nsec < 0) {
            real_time_now.tv_nsec += 1000000000; // + 1s
            real_time_now.tv_sec--;              // -1s
          }

          cpu_time_now.tv_sec -= cpu_time.tv_sec;
          cpu_time_now.tv_nsec -= cpu_time.tv_nsec;

          if (cpu_time_now.tv_nsec < 0) {
            cpu_time_now.tv_nsec += 1000000000; // + 1s
            cpu_time_now.tv_sec--;              // -1s
          }

          printf("REAL TIME: %lds, %ld\n", real_time_now.tv_sec,
                 real_time_now.tv_nsec);
          printf("CPU TIME: %lds, %ld\n", cpu_time_now.tv_sec,
                 cpu_time_now.tv_nsec);
        #endif

        pthread_exit(NULL);
      }

      INFO_THREAD("Doing work!");

      #if defined(THBOOST_TIME_INFO)
        clock_t current_time = clock();
        INFO_TIME("whole worker iteration took", current_time - last_time);
        last_time = current_time;
      #endif

      /* Execute the assigned function */
      (*func_ptr)((void *)*args_ptr);

      #if defined(THBOOST_TIME_INFO)
        current_time = clock();
        INFO_TIME("worker job took", current_time - last_time);
      #endif

      /* Set the stop condition so the main thread nows work is finished */
      *stop_condition_ptr = *start_condition_ptr;
    } else {

      #ifdef THBOOST_EVENT_DELAY
        THBOOST_EVENT_DELAY();
      #endif

      INFO_THREAD("Sleeping!");
    }
  }
}


/* ======================  INTERNAL THREADBOOST API  ====================== */

static thboost_t *internal_thboost = NULL;

void thboost_internal_init(const unsigned int num_of_threads) {
  if (internal_thboost == NULL) {
    internal_thboost = thboost_init(num_of_threads);
  }
}

void thboost_internal_work() { thboost_work(internal_thboost); }

void thboost_internal_add_work(const unsigned int thread_num, void *const func,
                               void *const args) {
  thboost_add_work(internal_thboost, thread_num, func, args);
}

void thboost_internal_add_work_all(void *const func, void *const args) {
  thboost_add_work_all(internal_thboost, func, args);
}

void thboost_internal_add_args(const unsigned int thread_num, void *const args) {
  thboost_add_args(internal_thboost, thread_num, args);
}

void thboost_internal_add_args_all(void *const args) {
  thboost_add_args_all(internal_thboost, args);
}

void thboost_internal_add_func(const unsigned int thread_num, void *const func) {
  thboost_add_func(internal_thboost, thread_num, func);
}

void thboost_internal_add_func_all(void *const func) {
  thboost_add_func_all(internal_thboost, func);
}

void thboost_internal_destroy() {
  thboost_destroy(internal_thboost);
  internal_thboost = NULL;
}
